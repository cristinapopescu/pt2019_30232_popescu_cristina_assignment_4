package BusinessLayer;

public interface IRestaurantProcessing {

     void createItem(MenuItem item);
     void deleteItem(MenuItem id);
     void editItem(MenuItem item);

     void createOrder(Order order);
     Double computePrice(Integer id);
     void generateBill();
}
