package BusinessLayer;


import java.util.*;

public class Restaurant implements IRestaurantProcessing {

    public HashMap<Order, ArrayList<MenuItem>> orderMap = new HashMap<>();
    public ArrayList<MenuItem> menu = new ArrayList<>();


    public Restaurant() {
        orderMap = new HashMap<Order, ArrayList<MenuItem>>();
    }

    public Restaurant(HashMap<Order, ArrayList<MenuItem>> orderMap) {
        this.orderMap = orderMap;
    }



    @Override
    public void createItem(MenuItem item) {
        menu.add(item);
    }

    @Override
    public void deleteItem(MenuItem item) {
        menu.remove(item);
    }

    @Override
    public void editItem(MenuItem item) {

    }

    public MenuItem getMenuItem(String name) {
        for (MenuItem item : menu) {
            if (item.getName().equals(name))
                return item;
        }
        return null;
    }

    public void addMenuItem(Integer orderID, MenuItem item) {
        Set<Order> set = orderMap.keySet();
        for(Order ord : set){
            if(ord.getOrderID().equals(orderID)){
                orderMap.get(ord).add(item);
                ord.setPrice(ord.getPrice() + item.getPrice());
            }
        }
    }

    @Override
    public void createOrder(Order order) {
        boolean ok = true;
        Set<Order> set = orderMap.keySet();
        for (Order ord : set) {
            if (ord.getOrderID().equals(order.getOrderID()))
                ok = false;
        }
        if (ok == true)
            orderMap.put(order, new ArrayList<>());
    }

    @Override
    public Double computePrice(Integer id) {
        Set<Order> set = orderMap.keySet();
        Double sum = new Double(0);

        for (Order ord : set) {
            if (ord.getOrderID().equals(id)) {
                List<MenuItem> list = orderMap.get(ord);
                for (MenuItem item : list)
                    sum += item.getPrice();
            }
        }
        return sum;
    }

    @Override
    public void generateBill() {

    }
}
