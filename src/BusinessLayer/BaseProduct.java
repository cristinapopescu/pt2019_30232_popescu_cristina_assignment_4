package BusinessLayer;

public class BaseProduct extends MenuItem {
    private String name;
    private double price;
    private int quantity;

    public BaseProduct(String name, double price, int quantity) {
        super(name, price, quantity);
    }

    @Override
    public void computePrice() {

    }
}
