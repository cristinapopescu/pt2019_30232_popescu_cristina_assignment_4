package BusinessLayer;

public class CompositeProduct extends MenuItem {
    private String name;
    private double price;
    private int quantity;
    public CompositeProduct(String name, double price, int quantity) {
        super(name, price, quantity);
    }

    @Override
    public void computePrice() {

    }
}
