package BusinessLayer;

public class Order {

    private Integer orderID;
    private Double price;

    public Order(Integer orderID, Double price) {
        this.orderID = orderID;
        this.price = price;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int hashCode() {
        return this.orderID;
    }
}
