package DataLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Bill {

    public void generateBill(Integer id, HashMap<Order, ArrayList<MenuItem>> map) {
        Set<Order> set = map.keySet();
        List<MenuItem> list = new ArrayList<>();
        Order order = null;

        for (Order ord : set) {
            if (ord.getOrderID().equals(id)) {
                list = map.get(ord);
                order = ord;
            }
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("bill.txt"));
            writer.write("Nota de plata");
            writer.newLine();

            for (MenuItem item : list) {
                writer.write(item.getName() + " " + item.getPrice());
                writer.newLine();
            }
            writer.write("Total: ");
            writer.write(order.getPrice().toString());
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
