package PresentationLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class WaiterGraphicalUserInterface extends Frame {

    public JFrame frame = new JFrame();
    public JTextField orderID = new JTextField();
    public JTextField itemName = new JTextField();
    public JButton addOrder = new JButton("Add Order");
    public JButton viewOrders = new JButton("Compute bill");

    public JTable table;
    JScrollPane pane;
    JPanel panel = new JPanel();

    public void createFrame() {
        JLabel id = new JLabel();
        JLabel name = new JLabel();


        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setSize(600, 600);
        panel.setLayout(null);

        id.setText("Order id:");
        id.setSize(300, 40);
        id.setLocation(10, 20);
        panel.add(id);

        orderID.setSize(200, 30);
        orderID.setLocation(80, 20);
        panel.add(orderID);

        name.setText("Name :");
        name.setSize(300, 40);
        name.setLocation(10, 100);
        panel.add(name);

        itemName.setSize(200, 30);
        itemName.setLocation(80, 100);
        panel.add(itemName);


        addOrder.setSize(150, 30);
        addOrder.setLocation(80, 370);
        panel.add(addOrder);

        viewOrders.setSize(150, 30);
        viewOrders.setLocation(80, 420);
        panel.add(viewOrders);

    }
    public void initialize(){
        table = new JTable();
        pane = new JScrollPane(table);
        panel.add(pane);
    }

    public void createTable(Set<Order> set){
        DefaultTableModel model = new DefaultTableModel();

        pane.setBounds(300, 10, 250, 500);
        Object[] columns = {"idOrder", "Price"};
        Object[][] objects = new Object[set.size()][];
        int i = 0;
        for (Order ord : set)
        {
            objects[i] = new Object[2];
            objects[i][0] = ord.getOrderID();
            objects[i][1] = ord.getPrice();
            i++;
        }

        model.setDataVector(objects, columns);
        //model.setColumnIdentifiers(columns);
        table.setModel(model);
        table.setBackground(Color.cyan);
        table.setForeground(Color.white);
        Font font = new Font("", 1, 22);
        table.setFont(font);
        table.setRowHeight(30);
        //panel.add(pane);

        model.setDataVector(objects, columns);
        model.setColumnIdentifiers(columns);
        model.fireTableStructureChanged();
    }
}
