package PresentationLayer;

import BusinessLayer.BaseProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import DataLayer.Bill;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class Main {

    public static void main(String[] args) {
        AdminGraphicalUserInterface adminFrame = new AdminGraphicalUserInterface();
        WaiterGraphicalUserInterface waiterFrame = new WaiterGraphicalUserInterface();
        adminFrame.initialize();
        waiterFrame.initialize();

        Restaurant restaurant = new Restaurant();
        Bill bill = new Bill();

        MenuItem m1 = new MenuItem("pui", 12.0, 20);
        MenuItem m2 = new BaseProduct("porc", 22, 30);

        restaurant.createItem(m1);
        restaurant.createItem(m2);

        restaurant.createOrder(new Order(1, 0.0));
        restaurant.createOrder(new Order(2, 0.0));
        restaurant.addMenuItem(1, m1);
        restaurant.addMenuItem(2, m2);




        adminFrame.createFrame();
        adminFrame.frame.setVisible(true);
        adminFrame.createTable(restaurant.menu);

        waiterFrame.createFrame();
        waiterFrame.frame.setVisible(true);
        waiterFrame.createTable(restaurant.orderMap.keySet());

        adminFrame.addProduct.addActionListener(o1-> {
            MenuItem item = new MenuItem(adminFrame.itemName.getText(), Double.parseDouble(adminFrame.itemPrice.getText()), 0);
            restaurant.createItem(item);
            System.out.println(restaurant.menu.size());
            restaurant.menu.forEach(e -> {
                System.out.println(e.getName() + e.getPrice());
            });
            adminFrame.createTable(restaurant.menu);
        });
        adminFrame.deleteProduct.addActionListener(o2->{
            MenuItem item = new MenuItem(adminFrame.itemName.getText(), Double.parseDouble(adminFrame.itemPrice.getText()), 0);
            restaurant.deleteItem(item);
            System.out.println(restaurant.menu.size());
            restaurant.menu.forEach(e -> {
                System.out.println(e.getName() + e.getPrice());
            });
            adminFrame.createTable(restaurant.menu);
        });
        adminFrame.editProduct.addActionListener(o3-> {

        });

        waiterFrame.addOrder.addActionListener(o4-> {
            //MenuItem m = new MenuItem(waiterFrame.itemName.getText(), 0.0, 1);
            String name = waiterFrame.itemName.getText();
            MenuItem m = restaurant.getMenuItem(name);
            restaurant.createOrder(new Order(Integer.parseInt(waiterFrame.orderID.getText()), 0.0));
            restaurant.addMenuItem(Integer.parseInt(waiterFrame.orderID.getText()), m);
            waiterFrame.createTable(restaurant.orderMap.keySet());

        });
        waiterFrame.viewOrders.addActionListener(o5-> {
            bill.generateBill(Integer.parseInt(waiterFrame.orderID.getText()), restaurant.orderMap);
        });
    }
}
