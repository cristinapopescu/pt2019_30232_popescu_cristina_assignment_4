package PresentationLayer;

import BusinessLayer.MenuItem;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;


import java.awt.*;
import java.util.List;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class AdminGraphicalUserInterface extends Frame {

    public JFrame frame = new JFrame();
    public JTextField itemName = new JTextField();
    public JTextField itemPrice = new JTextField();
    public JButton addProduct = new JButton("Add");
    public JButton editProduct = new JButton("Edit");
    public JButton deleteProduct = new JButton("Delete");
    public JButton viewProduct = new JButton("View");

    public JTable table;
    JScrollPane pane;
    JPanel panel = new JPanel();
    public void createFrame() {

        JLabel name = new JLabel();
        JLabel price = new JLabel();


        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setSize(600, 600);
        panel.setLayout(null);

        name.setText("Item name:");
        name.setSize(300, 40);
        name.setLocation(10, 20);
        panel.add(name);

        itemName.setSize(200, 30);
        itemName.setLocation(80, 20);
        panel.add(itemName);

        price.setText("Price :");
        price.setSize(300, 40);
        price.setLocation(10, 100);
        panel.add(price);

        itemPrice.setSize(200, 30);
        itemPrice.setLocation(80, 100);
        panel.add(itemPrice);

        addProduct.setSize(100, 30);
        addProduct.setLocation(80, 370);
        panel.add(addProduct);

        editProduct.setSize(100, 30);
        editProduct.setLocation(80, 420);
        panel.add(editProduct);

        deleteProduct.setSize(100, 30);
        deleteProduct.setLocation(80, 470);
        panel.add(deleteProduct);

        viewProduct.setSize(100, 30);
        viewProduct.setLocation(80, 520);
        panel.add(viewProduct);

    }

    public void initialize(){
        table = new JTable();
        pane = new JScrollPane(table);
        panel.add(pane);
    }

    public void createTable(List<MenuItem> items){
        DefaultTableModel model = new DefaultTableModel();
//        String[] columns = {"Name", "Price"};
//        String[][] dataValues = new String[items.size()][2];
//        for (int i = 0; i < items.size(); i++) {
//            String o1 = items.get(i).getName();
//            String o2 = items.get(i).getPrice().toString();
//
//            dataValues[i][0] = o1;
//            dataValues[i][1] = o2;
//        }
//        model.setDataVector(dataValues, columns);
//        model.setColumnIdentifiers(columns);
//        model.fireTableStructureChanged();

        pane.setBounds(300, 10, 250, 500);
        Object[] columns = {"Name", "Price"};
        Object[][] objects = new Object[items.size()][];
        for (int i = 0; i < items.size(); i++) {
            objects[i] = new Object[2];
            objects[i][0] = items.get(i).getName();
            objects[i][1] = items.get(i).getPrice();
        }

        model.setDataVector(objects, columns);
        //model.setColumnIdentifiers(columns);
        table.setModel(model);
        table.setBackground(Color.cyan);
        table.setForeground(Color.white);
        Font font = new Font("", 1, 22);
        table.setFont(font);
        table.setRowHeight(30);
        //panel.add(pane);

        model.setDataVector(objects, columns);
        model.setColumnIdentifiers(columns);
        model.fireTableStructureChanged();
    }
}
